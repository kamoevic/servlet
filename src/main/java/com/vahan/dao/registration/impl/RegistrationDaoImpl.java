package com.vahan.dao.registration.impl;

import com.vahan.dao.config.DbConfig;
import com.vahan.dao.registration.RegistrationDao;
import com.vahan.model.registration.RegistrationModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by Vahan
 * Date: 3/5/16.
 * Time: 2:47 PM
 */
public class RegistrationDaoImpl implements RegistrationDao {

    @Override
    public void register(final RegistrationModel model) {

        final Connection connection = DbConfig.getConnection();

        try {
            PreparedStatement statement =
                    connection.prepareStatement("INSERT INTO user" +
                            "(firstname, lastname, email, password) VALUES(?,?,?,?)");

            statement.setString(1, model.getFirstName());
            statement.setString(2, model.getLastName());
            statement.setString(3, model.getEmail());
            statement.setString(4, model.getPassword());

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
