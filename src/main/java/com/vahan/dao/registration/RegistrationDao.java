package com.vahan.dao.registration;

import com.vahan.model.registration.RegistrationModel;

/**
 * Created by Vahan
 * Date: 3/5/16.
 * Time: 2:46 PM
 */
public interface RegistrationDao {

    /**
     * Do registration
     * @param model registration model
     */
    void register(final RegistrationModel model);
}
