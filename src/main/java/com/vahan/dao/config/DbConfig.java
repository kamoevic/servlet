package com.vahan.dao.config;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Vahan
 * Date: 3/5/16.
 * Time: 3:03 PM
 */
public final class DbConfig {

    public static final String DB_URL = "jdbc:mysql://localhost:3306/LoginRegisterTestDb";

    public static final String DB_USERNAME = "root";

    public static final String DB_PASSWORD = "root";


    public static Connection getConnection() {

        Connection connection = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return connection;
    }
}
