package com.vahan.servlet.register;

import com.vahan.dao.registration.RegistrationDao;
import com.vahan.dao.registration.impl.RegistrationDaoImpl;
import com.vahan.model.registration.RegistrationModel;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Vahan
 * Date: 3/5/16.
 * Time: 2:36 PM
 */
public class RegistrationServlet extends HttpServlet {

    private static final long serialVersionUID = 2145798183298169565L;


    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {

        final RegistrationModel model = getRegistrationModelFromRequest(req);

        if (model == null) {
            // TODO: show error message
        } else {

            final RegistrationDao dao = new RegistrationDaoImpl();

            dao.register(model);

        }

    }


    private RegistrationModel getRegistrationModelFromRequest(final HttpServletRequest request) {

        final RegistrationModel model = new RegistrationModel();

        model.setFirstName(request.getParameter("firstname"));
        model.setLastName(request.getParameter("lastname"));
        model.setEmail(request.getParameter("email"));
        model.setPassword(request.getParameter("password"));

        if (!request.getParameter("password").equalsIgnoreCase(request.getParameter("repassword"))) {
            return null;
        }

        return model;
    }
}
