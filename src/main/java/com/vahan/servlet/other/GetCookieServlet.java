package com.vahan.servlet.other;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by vahan on 2/26/16.
 */
public class GetCookieServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies();

        PrintWriter pw = resp.getWriter();

        resp.setContentType("text/html");

        for (int i = 0; i < cookies.length; i++){

            String name = cookies[i].getName();

            String value = cookies[i].getValue();

            pw.println("name: " + name + "\nvalue: " + value );

         }

        pw.close();
    }
}
