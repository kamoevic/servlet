package com.vahan.servlet.other;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

/**
 * Created by vahan on 2/27/16.
 */
public class DateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession hs = req.getSession();

        Date date = (Date)hs.getAttribute("vahan");

        resp.setContentType("text/html");

        PrintWriter pw = resp.getWriter();

        if (date != null){

            pw.println("last access: " + date + "<br>");
        }

        date = new Date();

        hs.setAttribute("vahan",date);

        pw.println("current date: " + date);
    }
}
