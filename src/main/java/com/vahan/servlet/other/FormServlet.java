package com.vahan.servlet.other;

import com.mysql.fabric.jdbc.FabricMySQLDriver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by vahan on 3/5/16.
 */
public class FormServlet extends HttpServlet {

    private static final String URL = "jdbc:mysql://localhost:3306/LoginRegisterTestDb";

    private static final String USERNAME = "root";

    private static final String PASSWORD = "root";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String insertTemplate = "INSERT INTO form(name,phone,address) VALUES(?,?,?)";

        try {
            Driver driver = new FabricMySQLDriver();
            DriverManager.registerDriver(driver);
            Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            PreparedStatement insert = connection.prepareStatement(insertTemplate);
            insert.setString(1, req.getParameter("customerName"));
            insert.setString(2, req.getParameter("customerPhone"));
            insert.setString(3, req.getParameter("customerAddress"));
            insert.executeUpdate();
        } catch (SQLException s) {
            System.out.println(s);
            return;
        }

        resp.setContentType("text/html");
        PrintWriter pw = resp.getWriter();
        pw.println("Congratulations your registration was successfull");
    }
}
