package com.vahan.servlet.other;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by vahan on 3/4/16.
 */
public class TestMySQL extends HttpServlet {
    private static final String URL = "jdbc:mysql://localhost:3306/Table3";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter pw = resp.getWriter();

        try {
            Connection connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
        }catch (SQLException s){
            System.out.println(s);
        }

    }
}
