package com.vahan.servlet.other;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Iterator;

/**
 * Created by vahan on 2/24/16.
 */
public class Servlet extends GenericServlet {


    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();

        Enumeration e = request.getParameterNames();

        while (e.hasMoreElements()) {

            String pName = (String) e.nextElement();
            pw.write(pName + " = ");
            String pValue = request.getParameter(pName);
            pw.println(pValue);
        }
        pw.close();
    }
}
