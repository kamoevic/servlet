package com.vahan.servlet.other;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by vahan on 2/26/16.
 */
public class AddCookie extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req,
                          HttpServletResponse resp)
            throws ServletException, IOException {

        String data = req.getParameter("data");
        Cookie cookie = new Cookie("MyCookie",data);
        resp.addCookie(cookie);

        PrintWriter pw = resp.getWriter();
        pw.print("MyCookie has been set to ");
        pw.println(data);
        pw.close();
    }
}
