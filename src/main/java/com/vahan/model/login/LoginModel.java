package com.vahan.model.login;

import java.io.Serializable;

/**
 * Created by Vahan
 * Date: 3/5/16.
 * Time: 2:33 PM
 */
public class LoginModel implements Serializable {

    private static final long serialVersionUID = 3286869380589908604L;


    /*Properties*/
    private String email;

    private String password;


    /*Getters and setters*/
    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LoginModel)) return false;
        final LoginModel that = (LoginModel) o;
        return getEmail().equals(that.getEmail()) && getPassword().equals(that.getPassword());

    }

    @Override
    public int hashCode() {
        int result = getEmail().hashCode();
        result = 31 * result + getPassword().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "LoginModel{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
