package com.vahan.model.registration;

import java.io.Serializable;

/**
 * Created by Vahan
 * Date: 3/5/16.
 * Time: 2:43 PM
 */
public class RegistrationModel implements Serializable {

    private static final long serialVersionUID = 8046340752468876850L;


    /*Properties*/
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String password;



    /*Getters and setters*/
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
